# An API for the o2 HomeSpot

## I won't continue this, because I no longer own a o2 Homespot, since they destroyed the Firmware with the "DE_g34.0_RTL0080.D112.DE.21.13.115 [2020-10-15 12:14:02]" Update.


Does not support all functions yet. Feel free to contribute. <br />
This package uses node-fetch for requests.

# *CORS PROXY IS REQUIRED*
This API probably won't work, if you don't use a proxy. CORS errors will occur. <br />
Use for example [local-cors-proxy](https://www.npmjs.com/package/local-cors-proxy). <br />
Start it like this ```lcp --proxyUrl http://192.168.1.1``` (http://192.168.1.1 should be the ip of the router)

Then call the constructor with the url provided from lcp as the base url. <br />
Like `const api = new HomeSpotApi("http://localhost:8010/proxy");`

# Installation

```bash
$ npm install homespot-api --save
```


# Usage 
```javascript
const { HomespotApi } = require("homespot-api");
// import { HomespotApi } from "homespot-api"; ES6 import

const api = new HomespotApi("http://192.168.1.1");

api.login("Password").then((result) =>
{
    if (result)
    {
        console.log("Login successful");
    }
});

```

<br />
<br />
<br />
<br />

# Methods
[`login(password: string): Promise<boolean>`](#loginpassword-string-promiseboolean)

[`getMainStatus(): Promise<HomespotMainInfo>`](#getmainstatus-promisehomespotmaininfo)

[`getSystemInformation(): Promise<HomespotSystemInformation>`](#getsysteminformation-promisehomespotsysteminformation)

[`getWifiInformation(): Promise<HomespotWifiResult>`](#getwifiinformation-promisehomespotwifiresult)

[`getWifiPlusInformation(): Promise<HomespotWifiPlusResult>`](#getwifiplusinformation-promisehomespotwifiplusresult)

[`getPreferedConnectionType(): Promise<HomespotPreferedConnectionType>`](#getpreferedconnectiontype-promisehomespotpreferedconnectiontype)

[`setPreferedConnectionType(value: HomespotPreferedConnectionType): Promise<boolean>`](#setpreferedconnectiontypevalue-homespotpreferedconnectiontype-promiseboolean)

[`getLocalNetworkInformation(): Promise<HomespotLocalNetworkInfo>`](#getlocalnetworkinformation-promisehomespotlocalnetworkinfo)

[`restart(): void`](#restart-void)

<br />
<br />
<br />
<br />

# `login(password: string): Promise<boolean>`

Logs you into the homespot. The Password is the same you would use on the default router page (e.g "o2.spot" or "192.168.1.1")

<div align="center">
<p align="center">
<img align="center" src="https://gitlab.com/julianbaumann/homespot-api/raw/master/router-login-image.png" width="200"/>
</p>
</div>


``` typescript
api.login("Password").then((result) =>
{
    if (result)
    {
        console.log("Login successful");
    }
});
```

<br />
<br />
<br />
<br />

# `getMainStatus(): Promise<HomespotMainInfo>`

Returns a Promise with `HomespotMainInfo`:
```typescript
export class HomespotMainInfo
{
    public CellInfo: HomespotCellInfo;
    public RadioStatus: HomespotRadioStatus;
}

interface HomespotCellInfo
{
    RSRP: string;
    RSRQ: string;
    RSSI: string;
    TAC: string;
    PCI: string;
    CellId: string;
    EARFCN: string;
}

interface HomespotRadioStatus
{
    Provider: string;
    RadioStation: string;
    SignalStrength: string;
}
```

<br />
<br />
<br />
<br />

# `getSystemInformation(): Promise<HomespotSystemInformation>`
Returns a Promise with `HomespotSystemInformation`
``` typescript
{
    FirmwareName: string;
    FirmwareUpdateTime: string;
    HwVersion: string;
    DevManufacturer: string;
    Imei: string;
    SerialNumber: string;
    Uptime: string;
}
```
<br />
<br />
<br />
<br />

# `getWifiInformation(): Promise<HomespotWifiResult>`
Returns a Promise with `HomespotWifiResult`
```typescript
{
    Active: boolean;
    SSID: string;
    Mac: string;
    Encryption: string;
    BroadcastSSID: boolean;
    Channel: number | string; // just returns a string when the number conversion returns NaN
    ConnectedDevices: number | string; // just returns a string when the number conversion returns NaN
}
```
<br />
<br />
<br />
<br />

# `getWifiPlusInformation(): Promise<HomespotWifiPlusResult>`
Returns a Promise with `HomespotWifiPlusResult`
```typescript
{
    Active: boolean;
    SSID: string;
    Mac: string;
    Mode: string;
    Encryption: string;
    Channel: number | string; // just returns a string when the number conversion returns NaN
    ConnectedDevices: number | string; // just returns a string when the number conversion returns NaN
}
```
<br />
<br />
<br />
<br />

# `getPreferedConnectionType(): Promise<HomespotPreferedConnectionType>`
Returns a Promise with `HomespotPreferedConnectionType` (enum)
```typescript
export enum HomespotPreferedConnectionType
{
    Both = 0,
    LTE,
    ThirdGeneration
}
```
<br />
<br />
<br />
<br />

# `setPreferedConnectionType(value: HomespotPreferedConnectionType): Promise<boolean>`
Returns a boolean Promise. `true` if the action was successful.

Parameters: `value: HomespotPreferedConnectionType`

``` typescript
const { HomespotPreferedConnectionType } = require("homespot-api") 
// import { HomespotPreferedConnectionType } from "homespot-api" ES6 import

api.setPreferedConnectionType(HomespotPreferedConnectionType.LTE).then((result) =>
{
    if (result)
    {
        console.log("action successful");
    }
});
```

<br />
<br />
<br />
<br />

# `getLocalNetworkInformation(): Promise<HomespotLocalNetworkInfo>`
Returns a Promise with `HomespotLocalNetworkInfo`
```typescript
{
    LanIpAddress: string;
    Netmask: string;
    DHCPLeaseTime: string;
    DHCPStart: string;
    DHCPEnd: string;
    DHCPServerEnabled: boolean;
    EthernetMacAddress: string;
}
```

<br />
<br />
<br />
<br />

# `restart(): void`

Reboots the router. Returns nothing.
