"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HomespotApi = void 0;
const tslib_1 = require("tslib");
const node_fetch_1 = tslib_1.__importDefault(require("node-fetch"));
const main_info_1 = require("./interfaces/main-info");
const prefered_connection_type_1 = require("./interfaces/prefered-connection-type");
class HomespotApi {
    constructor(url = "http://192.168.1.1") {
        this._url = url;
    }
    get(url, resType = "text", headers = null) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const fullUrl = `${this._url}${url}`;
            const header = Object.assign({ responseType: resType }, headers);
            try {
                const response = yield node_fetch_1.default(fullUrl, {
                    headers: header
                });
                switch (resType) {
                    case "text":
                        return yield response.text();
                    case "json":
                    default:
                        return yield response.json();
                }
            }
            catch (error) {
                console.log(`[HomespotApi] Error in Get() ${fullUrl}`, error);
            }
        });
    }
    getValueInBetween(text, first, last) {
        let result;
        const splittedOnce = text ? text.split(first)[1] : null;
        if (splittedOnce) {
            result = splittedOnce ? splittedOnce.split(last)[0] : null;
        }
        return result;
    }
    login(password) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const leadingZero = (num) => {
                let s = String(num);
                while (s.length < 2) {
                    s = `0${s}`;
                }
                return s;
            };
            const date = new Date();
            const month = leadingZero(date.getMonth() + 1);
            const year = date.getFullYear().toString();
            const day = leadingZero(date.getDate());
            const result = yield this.get(`/cgi-bin/mycgi?ACT=AuthLogin&type=login&act=admin&pws=${password}&monthdate=${month}${day}&yearmonthdate=${year}%2F${month}%2F${day}`);
            if (result == 0) {
                return true;
            }
            return false;
        });
    }
    getMainStatus() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const result = new main_info_1.HomespotMainInfo();
            const rawCellInfo = yield this.get("/cgi-bin/mycgi?ACT=GetLTECellinfo");
            const cellInfo = rawCellInfo ? rawCellInfo.split(",") : null;
            if (!cellInfo) {
                return;
            }
            result.CellInfo = {
                RSRP: cellInfo[0],
                RSRQ: cellInfo[1],
                RSSI: cellInfo[2],
                TAC: cellInfo[3],
                PCI: cellInfo[4],
                CellId: cellInfo[5],
                EARFCN: cellInfo[6]
            };
            const radioStationName = (yield this.get("/cgi-bin/mycgi?ACT=GetNameRadioSts")).split(",");
            const deviceInfo = (yield this.get("/cgi-bin/mycgi?ACT=GetCnnSts_DevInfo")).split(",");
            result.RadioStatus = {
                Provider: radioStationName[1],
                RadioStation: radioStationName[5] ? radioStationName[5].replace(/(\r\n|\n|\r)/gm, "") : null,
                SignalStrength: deviceInfo[2]
            };
            return result;
        });
    }
    getSystemInformation() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const systemInfo = yield this.get("/cgi-bin/mycgi?ACT=system_dinformation.html");
            const firmwareVersion = this.getValueInBetween(systemInfo, "firmware_version = '", "';");
            const firmwareName = firmwareVersion.split(" [")[0];
            const firmwareUpdateTime = this.getValueInBetween(firmwareVersion, " [", "]");
            const hwVersion = this.getValueInBetween(systemInfo, "hw_version = '", "';");
            const devManufacturer = this.getValueInBetween(systemInfo, "dev_manufacturer = '", "';");
            const imei = this.getValueInBetween(systemInfo, "imei = '", "';");
            const serialNumber = this.getValueInBetween(systemInfo, "sn_version = '", "';");
            const uptimeInSeconds = this.getValueInBetween(systemInfo, "up_secs = '", "';");
            const result = {
                FirmwareName: firmwareName,
                FirmwareUpdateTime: firmwareUpdateTime,
                HwVersion: hwVersion,
                DevManufacturer: devManufacturer,
                Imei: imei,
                SerialNumber: serialNumber,
                Uptime: uptimeInSeconds
            };
            return result;
        });
    }
    getWifiInformation() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const wifiData = (yield this.get("/cgi-bin/mycgi?ACT=Get24gStatus")).split(",");
            // const passwordTwo: string = await this.GetWifiPassword("config_wireless24.php") || "";
            let twoPointFiveGHzEncryption = wifiData[3];
            if (wifiData[3] == "wpa2_aes") {
                twoPointFiveGHzEncryption = "WPA2-AES";
            }
            return {
                Active: wifiData[0] == "0" ? false : true,
                SSID: wifiData[1] || null,
                Mac: wifiData[5],
                Encryption: twoPointFiveGHzEncryption,
                BroadcastSSID: wifiData[2] == "0" ? false : true,
                Channel: isNaN(Number(wifiData[6])) ? wifiData[6] : Number(wifiData[6]),
                ConnectedDevices: isNaN(Number(wifiData[7])) ? wifiData[7] : Number(wifiData[7])
                // Password: passwordTwo
            };
        });
    }
    getWifiPlusInformation() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const wifiPlusData = (yield this.get("/cgi-bin/mycgi?ACT=Get5gStatus")).split(",");
            // const passwordFive: string = await this.GetWifiPassword("config_wireless.php") || "";
            let fiveGHzEncryption = wifiPlusData[4];
            if (wifiPlusData[4] == "11i") {
                fiveGHzEncryption = "WPA2-AES";
            }
            return {
                Active: wifiPlusData[0] == "0" ? false : true,
                SSID: wifiPlusData[1] || null,
                Mac: wifiPlusData[7],
                Mode: wifiPlusData[2],
                Encryption: fiveGHzEncryption,
                Channel: isNaN(Number(wifiPlusData[8])) ? wifiPlusData[8] : Number(wifiPlusData[8]),
                ConnectedDevices: isNaN(Number(wifiPlusData[9])) ? wifiPlusData[9] : Number(wifiPlusData[9])
                // Password: passwordFive
            };
        });
    }
    getIndividualWifiInfo() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            console.error("GetIndividualWifiInfo() is deprecated. Use GetWifiPlusInformations() and GetWifiInformations() instead");
        });
    }
    getPreferedConnectionType() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const result = yield this.get("/cgi-bin/mycgi?ACT=mobile_network.html");
            const status = this.getValueInBetween(result, "PrefSys	  = '", "';") || "0";
            return prefered_connection_type_1.HomespotPreferedConnectionType[status];
        });
    }
    setPreferedConnectionType(value) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const type = String(Number(value));
            const result = yield this.get(`/cgi-bin/mycgi?ACT=SetWan&prefCnnMode=${type}`);
            if (result == 0) {
                return true;
            }
            return false;
        });
    }
    getLocalNetworkInformation() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const rawLanInfo = yield this.get("/cgi-bin/mycgi?ACT=lan.html", "text");
            const lan = this.getValueInBetween(rawLanInfo, "CfgLan = '", "';");
            const netmask = this.getValueInBetween(rawLanInfo, "CfgNetmask = '", "';");
            const lease = this.getValueInBetween(rawLanInfo, "CfgLease = '", "';");
            const dhcpStart = this.getValueInBetween(rawLanInfo, "CfgDhcpStart = '", "';");
            const dhcpEnd = this.getValueInBetween(rawLanInfo, "CfgDhcpEnd = '", "';");
            const dhcpS = this.getValueInBetween(rawLanInfo, "CfgDhcpS = '", "';");
            const macAddress = this.getValueInBetween(rawLanInfo, "macaddr_lan = '", "';");
            return {
                LanIpAddress: lan,
                Netmask: netmask,
                DHCPLeaseTime: lease,
                DHCPStart: dhcpStart,
                DHCPEnd: dhcpEnd,
                DHCPServerEnabled: dhcpS == "0" ? false : true,
                EthernetMacAddress: macAddress
            };
        });
    }
    restart() {
        this.get("/cgi-bin/mycgi?ACT=SystemReboot");
    }
}
exports.HomespotApi = HomespotApi;
//# sourceMappingURL=api.js.map