export interface HomespotSystemInformation
{
    FirmwareName: string;
    FirmwareUpdateTime: string;
    HwVersion: string;
    DevManufacturer: string;
    Imei: string;
    SerialNumber: string;
    Uptime: string;
}