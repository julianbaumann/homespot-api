export declare enum HomespotPreferedConnectionType {
    Both = 0,
    LTE = 1,
    ThirdGeneration = 2
}
