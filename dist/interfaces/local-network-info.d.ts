export interface HomespotLocalNetworkInfo {
    LanIpAddress: string;
    Netmask: string;
    DHCPLeaseTime: string;
    DHCPStart: string;
    DHCPEnd: string;
    DHCPServerEnabled: boolean;
    EthernetMacAddress: string;
}
