export interface HomespotCellInfo
{
    RSRP: string;
    RSRQ: string;
    RSSI: string;
    TAC: string;
    PCI: string;
    CellId: string;
    EARFCN: string;
}

export interface HomespotRadioStatus
{
    Provider: string;
    RadioStation: string;
    SignalStrength: string;
}

export class HomespotMainInfo
{
    public CellInfo: HomespotCellInfo;
    public RadioStatus: HomespotRadioStatus;
}