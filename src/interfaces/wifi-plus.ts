export interface HomespotWifiPlusResult
{
    Active: boolean;
    SSID: string;
    Mac: string;
    Mode: string;
    Encryption: string;
    Channel: number | string;
    ConnectedDevices: number | string;
    // Password: string;
}