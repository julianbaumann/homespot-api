export enum HomespotPreferedConnectionType
// eslint-disable-next-line @typescript-eslint/indent
{
    Both = 0,
    LTE,
    ThirdGeneration
}