export interface HomespotWifiResult
{
    Active: boolean;
    SSID: string;
    Mac: string;
    Encryption: string;
    BroadcastSSID: boolean;
    Channel: number | string;
    ConnectedDevices: number | string;
    // Password: string;
}