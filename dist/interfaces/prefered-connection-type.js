"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HomespotPreferedConnectionType = void 0;
var HomespotPreferedConnectionType;
(function (HomespotPreferedConnectionType) {
    HomespotPreferedConnectionType[HomespotPreferedConnectionType["Both"] = 0] = "Both";
    HomespotPreferedConnectionType[HomespotPreferedConnectionType["LTE"] = 1] = "LTE";
    HomespotPreferedConnectionType[HomespotPreferedConnectionType["ThirdGeneration"] = 2] = "ThirdGeneration";
})(HomespotPreferedConnectionType = exports.HomespotPreferedConnectionType || (exports.HomespotPreferedConnectionType = {}));
//# sourceMappingURL=prefered-connection-type.js.map