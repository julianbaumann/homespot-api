import fetch from "node-fetch";

import { HomespotLocalNetworkInfo } from "./interfaces/local-network-info";
import { HomespotMainInfo } from "./interfaces/main-info";
import { HomespotPreferedConnectionType } from "./interfaces/prefered-connection-type";
import { HomespotSystemInformation } from "./interfaces/system-info";
import { HomespotWifiResult } from "./interfaces/wifi";
import { HomespotWifiPlusResult } from "./interfaces/wifi-plus";

export class HomespotApi
{
    private _url: string;

    constructor(url: string = "http://192.168.1.1")
    {
        this._url = url;
    }

    private async get(url: string, resType: string = "text", headers: any = null): Promise<any>
    {
        const fullUrl: string = `${this._url}${url}`;

        const header: any = {
            responseType: resType,
            ...headers
        };

        try
        {
            const response: any = await fetch(fullUrl, {
                headers: header
            });

            switch(resType)
            {
                case "text":
                    return await response.text();

                case "json":
                default:
                    return await response.json();
            }

        }
        catch (error)
        {
            console.log(`[HomespotApi] Error in Get() ${fullUrl}`, error);
        }
    }

    private getValueInBetween(text: string, first: string, last: string): string
    {
        let result: string;

        const splittedOnce: string = text ? text.split(first)[1] : null;

        if (splittedOnce)
        {
            result = splittedOnce ? splittedOnce.split(last)[0] : null;
        }

        return result;
    }

    public async login(password: string): Promise<boolean>
    {
        const leadingZero: Function = (num: number) =>
        {
            let s: string = String(num);
            while (s.length < 2)
            {
                s = `0${s}`;
            }
            return s;
        };
        const date: Date = new Date();

        const month: string = leadingZero(date.getMonth() + 1);
        const year: string = date.getFullYear().toString();
        const day: string = leadingZero(date.getDate());

        const result: any = await this.get(`/cgi-bin/mycgi?ACT=AuthLogin&type=login&act=admin&pws=${password}&monthdate=${month}${day}&yearmonthdate=${year}%2F${month}%2F${day}`);

        if (result == 0)
        {
            return true;
        }
        return false;
    }

    public async getMainStatus(): Promise<HomespotMainInfo>
    {
        const result: HomespotMainInfo = new HomespotMainInfo();

        const rawCellInfo: any = await this.get("/cgi-bin/mycgi?ACT=GetLTECellinfo");

        const cellInfo: string = rawCellInfo ? rawCellInfo.split(",") : null;

        if (!cellInfo)
        {
            return;
        }

        result.CellInfo = {
            RSRP: cellInfo[0],
            RSRQ: cellInfo[1],
            RSSI: cellInfo[2],
            TAC: cellInfo[3],
            PCI: cellInfo[4],
            CellId: cellInfo[5],
            EARFCN: cellInfo[6]
        };

        const radioStationName: string = (await this.get("/cgi-bin/mycgi?ACT=GetNameRadioSts")).split(",");

        const deviceInfo: string = (await this.get("/cgi-bin/mycgi?ACT=GetCnnSts_DevInfo")).split(",");


        result.RadioStatus = {
            Provider: radioStationName[1],
            RadioStation: radioStationName[5] ? radioStationName[5].replace(/(\r\n|\n|\r)/gm, "") : null,
            SignalStrength: deviceInfo[2]
        };

        return result;
    }

    public async getSystemInformation(): Promise<HomespotSystemInformation>
    {
        const systemInfo: string = await this.get("/cgi-bin/mycgi?ACT=system_dinformation.html");

        const firmwareVersion: string = this.getValueInBetween(systemInfo, "firmware_version = '", "';");
        const firmwareName: string = firmwareVersion.split(" [")[0];
        const firmwareUpdateTime: string = this.getValueInBetween(firmwareVersion, " [", "]");

        const hwVersion: string = this.getValueInBetween(systemInfo, "hw_version = '", "';");
        const devManufacturer: string = this.getValueInBetween(systemInfo, "dev_manufacturer = '", "';");
        const imei: string = this.getValueInBetween(systemInfo, "imei = '", "';");
        const serialNumber: string = this.getValueInBetween(systemInfo, "sn_version = '", "';");
        const uptimeInSeconds: string = this.getValueInBetween(systemInfo, "up_secs = '", "';");

        const result: HomespotSystemInformation = {
            FirmwareName: firmwareName,
            FirmwareUpdateTime: firmwareUpdateTime,
            HwVersion: hwVersion,
            DevManufacturer: devManufacturer,
            Imei: imei,
            SerialNumber: serialNumber,
            Uptime: uptimeInSeconds
        };

        return result;
    }

    public async getWifiInformation(): Promise<HomespotWifiResult>
    {
        const wifiData: string = (await this.get("/cgi-bin/mycgi?ACT=Get24gStatus")).split(",");
        // const passwordTwo: string = await this.GetWifiPassword("config_wireless24.php") || "";

        let twoPointFiveGHzEncryption: string = wifiData[3];

        if (wifiData[3] == "wpa2_aes")
        {
            twoPointFiveGHzEncryption = "WPA2-AES";
        }

        return {
            Active: wifiData[0] == "0" ? false : true,
            SSID: wifiData[1] || null,
            Mac: wifiData[5],
            Encryption: twoPointFiveGHzEncryption,
            BroadcastSSID: wifiData[2] == "0" ? false : true,
            Channel: isNaN(Number(wifiData[6])) ? wifiData[6] : Number(wifiData[6]),
            ConnectedDevices: isNaN(Number(wifiData[7])) ? wifiData[7] : Number(wifiData[7])
            // Password: passwordTwo
        };
    }

    public async getWifiPlusInformation(): Promise<HomespotWifiPlusResult>
    {
        const wifiPlusData: Array<string> = (await this.get("/cgi-bin/mycgi?ACT=Get5gStatus")).split(",");
        // const passwordFive: string = await this.GetWifiPassword("config_wireless.php") || "";

        let fiveGHzEncryption: string = wifiPlusData[4];

        if (wifiPlusData[4] == "11i")
        {
            fiveGHzEncryption = "WPA2-AES";
        }

        return {
            Active: wifiPlusData[0] == "0" ? false : true,
            SSID: wifiPlusData[1] || null,
            Mac: wifiPlusData[7],
            Mode: wifiPlusData[2],
            Encryption: fiveGHzEncryption,
            Channel: isNaN(Number(wifiPlusData[8])) ? wifiPlusData[8] : Number(wifiPlusData[8]),
            ConnectedDevices: isNaN(Number(wifiPlusData[9])) ? wifiPlusData[9] : Number(wifiPlusData[9])
            // Password: passwordFive
        };
    }

    public async getIndividualWifiInfo(): Promise<void>
    {
        console.error("GetIndividualWifiInfo() is deprecated. Use GetWifiPlusInformations() and GetWifiInformations() instead");
    }

    public async getPreferedConnectionType(): Promise<HomespotPreferedConnectionType>
    {
        const result: any = await this.get("/cgi-bin/mycgi?ACT=mobile_network.html");
        const status: string = this.getValueInBetween(result, "PrefSys	  = '", "';") || "0";

        return HomespotPreferedConnectionType[status];
    }

    public async setPreferedConnectionType(value: HomespotPreferedConnectionType): Promise<boolean>
    {
        const type: string = String(Number(value));

        const result: any = await this.get(`/cgi-bin/mycgi?ACT=SetWan&prefCnnMode=${type}`);

        if (result == 0)
        {
            return true;
        }

        return false;
    }

    public async getLocalNetworkInformation(): Promise<HomespotLocalNetworkInfo>
    {
        const rawLanInfo: string = await this.get("/cgi-bin/mycgi?ACT=lan.html", "text");

        const lan: string = this.getValueInBetween(rawLanInfo, "CfgLan = '", "';");
        const netmask: string = this.getValueInBetween(rawLanInfo, "CfgNetmask = '", "';");
        const lease: string = this.getValueInBetween(rawLanInfo, "CfgLease = '", "';");
        const dhcpStart: string = this.getValueInBetween(rawLanInfo, "CfgDhcpStart = '", "';");
        const dhcpEnd: string = this.getValueInBetween(rawLanInfo, "CfgDhcpEnd = '", "';");
        const dhcpS: string = this.getValueInBetween(rawLanInfo, "CfgDhcpS = '", "';");
        const macAddress: string = this.getValueInBetween(rawLanInfo, "macaddr_lan = '", "';");

        return {
            LanIpAddress: lan,
            Netmask: netmask,
            DHCPLeaseTime: lease,
            DHCPStart: dhcpStart,
            DHCPEnd: dhcpEnd,
            DHCPServerEnabled: dhcpS == "0" ? false : true,
            EthernetMacAddress: macAddress
        };
    }

    public restart(): void
    {
        this.get("/cgi-bin/mycgi?ACT=SystemReboot");
    }
}
