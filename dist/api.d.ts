import { HomespotLocalNetworkInfo } from "./interfaces/local-network-info";
import { HomespotMainInfo } from "./interfaces/main-info";
import { HomespotPreferedConnectionType } from "./interfaces/prefered-connection-type";
import { HomespotSystemInformation } from "./interfaces/system-info";
import { HomespotWifiResult } from "./interfaces/wifi";
import { HomespotWifiPlusResult } from "./interfaces/wifi-plus";
export declare class HomespotApi {
    private _url;
    constructor(url?: string);
    private get;
    private getValueInBetween;
    login(password: string): Promise<boolean>;
    getMainStatus(): Promise<HomespotMainInfo>;
    getSystemInformation(): Promise<HomespotSystemInformation>;
    getWifiInformation(): Promise<HomespotWifiResult>;
    getWifiPlusInformation(): Promise<HomespotWifiPlusResult>;
    getIndividualWifiInfo(): Promise<void>;
    getPreferedConnectionType(): Promise<HomespotPreferedConnectionType>;
    setPreferedConnectionType(value: HomespotPreferedConnectionType): Promise<boolean>;
    getLocalNetworkInformation(): Promise<HomespotLocalNetworkInfo>;
    restart(): void;
}
